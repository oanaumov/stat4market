<?php

namespace AppPHP\Tasks;

use AppPHP\DB\Connection;
use DateTimeImmutable;

class TaskThree extends Connection
{
    public function __construct()
    {
        echo '<h1>Task Three</h1><br>';

        $this->getCountSearchWeeksDayInPeriod();
    }

    private function getCountSearchWeeksDayInPeriod(int $searchDay = 2 /* Вторник */, string $start = '2022-10-10', string $end = '2023-10-10'): void
    {
        $startDate = new DateTimeImmutable($start);
        $endDate = new DateTimeImmutable($end);

        $count = 0;
        while ($startDate <= $endDate) {
            if ($startDate->format('N') == $searchDay) {
                $count++;
            }

            $startDate = $startDate->modify('+1 day');
        }

        echo "Количество искомых дней недели ({$searchDay}) между {$start} и {$end}: {$count}";
    }
}
